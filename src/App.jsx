import { Route, Routes } from "react-router-dom"
import HomePage from "./pages/HomePage"
import KelasPage from "./pages/KelasPage"
import TestimonialPage from "./pages/TestimonialPage"
import SyaratKetenPage from "./pages/SyaratKetenPage"
import FaqComponent from "./components/FaqComponent"
import NavbarComponent from "./components/NavbarComponent"
import FooterComponent from "./components/FooterComponent"

function App() {
 return <div>
  <NavbarComponent/>
  <Routes>
    <Route path="/" Component={HomePage}></Route>
    <Route path="/kelas" Component={KelasPage}></Route>
    <Route path="/testimonial" Component={TestimonialPage}></Route>
    <Route path="/faq" Component={SyaratKetenPage}></Route>
    <Route path="/syaratketen" Component={FaqComponent}></Route>
  </Routes>
  <FooterComponent/>
 </div>
}

export default App
